package com.santanagilbert.miprimeraapp4a;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActividadSensorLuz extends AppCompatActivity implements SensorEventListener {

    TextView luz;
    SensorManager sensorManager;
    Sensor sensorLuz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_actividad_sensor_luz );

        luz = (TextView) findViewById( R.id.tvLuz );

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorLuz = sensorManager.getDefaultSensor( Sensor.TYPE_LIGHT );

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        luz.setText( "hola" );
        luz.setText(Float.toString( sensorEvent.values[0]));
        if (sensorEvent.values[0] < sensorLuz.getMaximumRange()){
            getWindow().getDecorView().setBackgroundColor(Color.GREEN);

        }
        else{
            getWindow().getDecorView().setBackgroundColor(Color.GRAY);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener( this,sensorLuz,sensorManager.SENSOR_DELAY_NORMAL );
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener( this );
    }

}
