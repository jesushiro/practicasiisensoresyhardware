package com.santanagilbert.miprimeraapp4a;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ActividadSensorProximidad extends AppCompatActivity implements SensorEventListener {

    SensorManager sensorManager;
    Sensor sensorProximidad;

    TextView textViewProximidad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensor_proximidad );

        textViewProximidad = (TextView) findViewById(R.id.tvProximidad);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorProximidad = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
    }

    @Override
    protected void onResume() {
        super.onResume();

        sensorManager.registerListener(this, sensorProximidad, sensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        textViewProximidad.setText(Float.toString( event.values[0]));
        if (event.values[0] < sensorProximidad.getMaximumRange()){
            getWindow().getDecorView().setBackgroundColor(Color.GREEN);

        }
        else{
            getWindow().getDecorView().setBackgroundColor(Color.GRAY);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
