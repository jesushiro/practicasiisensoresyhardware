package com.santanagilbert.miprimeraapp4a;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button botonLogin, botonRegistar, botonBuscar, botonParametro, botonfragmento, botonSensor, botonVibrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonParametro = (Button) findViewById(R.id.btnapasarparametro);
        botonLogin = (Button) findViewById(R.id.btnlogin);
        botonRegistar = (Button) findViewById(R.id.btnguardar);
        botonBuscar = (Button) findViewById(R.id.btnbuscar);
        botonfragmento = (Button) findViewById(R.id.btnfragmento);
        botonSensor = (Button) findViewById( R.id.btnSensor );
        botonVibrar = (Button) findViewById( R.id.btnVibrar );
//
        botonParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PasarParametro.class);
                startActivity(intent);
            }
        });

        botonfragmento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Fragmentos.class);
                startActivity(intent);
            }
        });
//
        botonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(MainActivity.this, ActividadLogin.class);
                startActivity(intent);

            }
        });

        botonRegistar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(MainActivity.this, ActividadRegistrar.class);
                startActivity(intent);

            }


        });

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(MainActivity.this, ActividadBuscar.class);
                startActivity(intent);

            }
        });

        botonSensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadSensorAcelerometro.class);
                startActivity(intent);
            }
        });

        final Vibrator vibrator = (Vibrator)this.getSystemService( Context.VIBRATOR_SERVICE );

        botonVibrar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vibrator.vibrate( 600 );
            }
        } );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(MainActivity.this);
                dialogoLogin.setContentView(R.layout.dlg_login);
                dialogoLogin.show();
                break;
        }

        switch (item.getItemId()){
            case R.id.opcionRegistrar:
                Dialog dialogoRegistar = new Dialog(MainActivity.this);
                dialogoRegistar.setContentView(R.layout.dlg_registrar);
                dialogoRegistar.show();
                break;
        }

        switch (item.getItemId()){
            case R.id.opcionProcimidad:
                intent = new Intent(MainActivity.this, ActividadSensorProximidad.class);
                startActivity(intent);
                break;
        }
        switch (item.getItemId()){
            case R.id.opcionLuz:
                intent = new Intent(MainActivity.this, ActividadSensorLuz.class);
                startActivity(intent);
                break;
        }

        return true;

    }



}
